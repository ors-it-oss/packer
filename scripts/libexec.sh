#!/bin/bash
[[ "$DEBUG" =~ shell || "$DEBUG" =~ all ]] && set -x

set -a
CHECKPOINT_DISABLE=1
PACKER_CACHE_DIR="$PACKER_ROOT/cache"
PACKER_CONFIG_DIR="$PACKER_ROOT/etc"
[[ "$DEBUG" =~ packer || "$DEBUG" =~ all ]] && PACKER_LOG=1
PACKER_PLUGIN_PATH="$PACKER_ROOT/plugins"
PKR_VAR_build_root="$PACKER_ROOT/build-$(date +"%s%N")"
PKR_TMP_DIR="$PKR_VAR_build_root"
PKR_PROV_DIR="$PKR_VAR_build_root/prov"
set +a

packer_build(){
	export tpl_dir="$1"

	prov "$tpl_dir"
	efi_vars
	oemdrv "$PKR_PROV_DIR" cdrom

	set -a
	packer_args="${PACKER_ONLY:+--only=$PACKER_ONLY}${PACKER_EXCLUDE:+--only=$PACKER_EXCLUDE}"
	for packer_var in $PACKER_VARS; do
		if [[ -f "$packer_var" ]]; then
			packer_args="$packer_args --var-file $packer_var"
		else
			eval PKR_VAR_$packer_var=$packer_var
	fi; done
	packer_args="$packer_args $tpl_dir"

	# https://github.com/hashicorp/packer/pull/9905
	sess_vnc=$(free_port 5900 6400)
	sess_http=$(free_port 5900 6400)
	PKR_VAR_http_port_min=$sess_http
	PKR_VAR_http_port_max=$sess_http
	PKR_VAR_vnc_port_min=$sess_vnc
	PKR_VAR_vnc_port_max=$sess_vnc
	set +a

	if [[ -z "$PACKER_CNT" ]]; then (
			export TMPDIR=$PKR_TMP_DIR
			exec packer build --var headless=false $packer_args
	)	else
		echo "$DEBUG" | grep -q -e all -e cri && cri_ll=debug
		podman --log-level ${cri_ll:-info} \
			run --rm -ti --device /dev/kvm -p $sess_vnc:$sess_vnc --hostname $tpl_dir \
			--security-opt label=disable \
			--env TMPDIR="$PKR_TMP_DIR" \
			${DEBUG:+--env DEBUG="$DEBUG" }${PACKER_LOG:+--env PACKER_LOG="$PACKER_LOG" } \
			$(for pkr_var in $(env|grep ^PKR_VAR_); do echo -n "--env $pkr_var "; done) \
		  --volume $PWD:/build:z \
		  --workdir /build \
		  --volume $PACKER_CONFIG_DIR:/etc/packer.d:z \
		  --volume $PACKER_CACHE_DIR:/var/packer/cache:z \
		  --volume $PKR_VAR_build_root:$PKR_VAR_build_root:z \
	  $PACKER_IMG packer build --var headless=true $packer_args
	fi
}

con(){
	# Find a Packer build process
	# Dig up it's VNC details in the logs
	# Open a VNCviewer
	build=$1

	packer_pid=$(pgrep -of "^[^ ]*packer build.*$build")
	[[ -n "$packer_pid" ]] || { echo "No Packer"; exit 1; }

	packer_log=$(lsof -n -F n -p $packer_pid | sed '/packer-log/!d;s|^n||')
	if grep -q container /proc/$packer_pid/cgroup; then
		ns="sudo nsenter -a -t $packer_pid"
	fi
	con=$($ns sed -E -e '/.*qemu.*vnc:\/\//!d' -e 's|.*vnc://([0-9.:]+).*|\1|' "$packer_log")
	[[ -n "$con" ]] || { echo "No VNC"; exit 1; }
	pass=$($ns sed -E -e '/.*qemu.*password:/!d' -e 's|.*password: ||' "$packer_log")

	vncviewer -Shared -SendClipboard ${pass:+-passwd <(echo $pass | vncpasswd -f -)} $con
}


####---------------- Connectivity ----------------####
ssh_key(){
	ssh-keygen -C "packer" -f $PKR_VAR_build_root/packer_ssh -t ed25519 -N ""
	mv "$PKR_VAR_build_root/packer_ssh.pub" "$PKR_PROV_DIR/packer_ssh.pub"
}

free_port(){
	max=$2
	min=$1
	range=$[$max - $min]
	size=$[$range * 2]
	while (($size > 0)); do
		port=$[$min + ($RANDOM % $range)]
		{ echo "" >/dev/tcp/127.0.0.1/${port}; } >/dev/null 2>&1
		(( $? != 0 )) && { echo $port; return; }
		size=$[$size - 1]
	done
	echo "No free port found between $max and $min after $[$range * 2] attempts"
	exit 1
}


####---------------- EFI ----------------####
efi_vars(){
	# Spool in new copy of EFI vars for easy modification
	cp /usr/share/OVMF/OVMF_VARS.fd "$PKR_TMP_DIR/efivars.fd"
}

efi_vars_modify(){
	# TODO: Does this even make sense??
	# now whut
	sudo mount -o loop,ro -t efivarfs /usr/share/OVMF/OVMF_VARS.fd /mnt
	sudo mount -o loop -t efivarfs .build/efivars.fd /mnt

	efivar -l -f /usr/share/OVMF/OVMF_VARS.fd
	# efivar

	# Remove immutable bit, allows modification
	chattr -i /sys/firmware/efi/efivars/SystemAudioVolume-7c436110-ab2a-4bbb-a880-fe41995c9f82

	# Set volume to 00
	printf "\x07\x00\x00\x00\x00" > /sys/firmware/efi/efivars/SystemAudioVolume-7c436110-ab2a-4bbb-a880-fe41995c9f82

	# !!1!!1!!	--qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=${IGNITION_CONFIG}"
}


####---------------- Pre-Provisioning ----------------####
prov(){
	tpl_dir=${1:-$tpl_dir}
	mkdir -p "$PKR_PROV_DIR"
	ssh_key
	cat <<-PROVENV >"$PKR_PROV_DIR/packer.env"
		http="http://10.0.2.2:$sess_http"
	PROVENV
	[[ -d "$tpl_dir/prov" ]] &&	cp -a "$tpl_dir"/prov/* $PKR_PROV_DIR/
	[[ -x "$tpl_dir/prov.sh" ]] && . "$tpl_dir/prov.sh"
}

oem_clean(){
	rm -f "$PACKER_CACHE/oemdrv.*"
}

oemdrv(){
	# Generate an oem drive
	# $1 = src
	# $2 = iso|disk|floppy
	oem_img="$PKR_VAR_build_root/oemdrv"
	oem_label=OEMDRV
	dir="$1"
	format="$2"
	[[ -d "$dir" ]] || { echo "No dir $dir"; exit 1; }
	case $format in
		cdrom) oem_img="${oem_img}.iso" ;;
		disk) oem_img="${oem_img}.img" ;;
		floppy) oem_img="${oem_img}.fd" ;;
		*) echo "Unknown oemdrv export format $format"; exit 1 ;;
	esac
	oem_$format
}

oem_cdrom(){
	# nonbootable ISO
	genisoimage -v -input-charset utf8 -J -joliet-long -r -V "$oem_label" -o "$oem_img" "$dir"
}

oem_disk(){
	# (USB) raw image
	exit 1
	mdir=$(mktemp -d)
	fd_label=$(echo "$oem_label" | tr '[:lower:]' '[:upper:]')
	mkfs.xfs -L "$oem_label" "$oem_img"
	mount -o loop "$oem_img" "$mdir"
	cp -a --no-preserve=ownership "${dir}/*" "$mdir"
	umount "$mdir"
	rmdir "$mdir"
	# Disk w/o root
	# https://wiki.osdev.org/UEFI#Creating_disk_images
  # https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface#Mount_efivarfs

}

oem_floppy(){
	# Why do we still use these things for anything
	exit 1
	mdir=$(mktemp -d)
	mkfs.fat -v -n "$oem_label" -C "$oem_img" 1440
	mount -o loop "$oem_img" "$mdir"
	cp -a --no-preserve=ownership "${dir}/*" "$mdir"
	umount "$mdir"
	rmdir "$mdir"
}


####---------------- execute args if not sourced ----------------####
[[ "${BASH_SOURCE[0]}" == "${0}" ]] && "$@"
