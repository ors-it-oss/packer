source qemu freebsd {
// DEVICES
  net_device = "virtio-net"
  disk_cache = "unsafe"
  disk_detect_zeroes = "on"
  disk_compression = true
  disk_discard = "unmap"
  disk_interface = "virtio"
  disk_size = var.disk_size
  format = "qcow2"

// QEMU
  accelerator = var.accelerator
  headless = var.headless
  //  display = "none"
  //  display = "gtk"
  //  use_default_display = true
  vnc_port_max = var.vnc_port_max
  vnc_port_min = var.vnc_port_min
  vnc_use_password = true
  qemuargs = [
    [ "-m", var.memory ],
    [ "-smp", "cpus=${var.cpus}" ],
    [ "-serial", "none"],  // QEMU automagically inserts a serial port, FreeBSD takes it and freezes there on req what tty you want
    [ "-fw_cfg", "name=opt/io.packer/url,string=http://{{ .HTTPIP }}:{{ .HTTPPort }}" ],
    [ "-drive", "id=drive0,file=${var.build_root}/out/${var.os_name}/${var.os_name}-${var.os_version},if=virtio,cache=unsafe,discard=unmap,format=qcow2,detect-zeroes=on" ],
    [ "-drive", "id=efifw,if=pflash,format=raw,readonly,file=/usr/share/OVMF/OVMF_CODE.fd" ],
//    [ "-drive", "id=efivar,if=pflash,format=raw,file=${var.build_root}/efivars.fd" ],
    [ "-drive", "id=oem,file=${var.build_root}/oemdrv.iso,if=virtio,media=cdrom,readonly" ]
  ]
  qmp_enable = true

// ISO
  iso_checksum = var.iso_checksum
  iso_url = var.iso_url

// BUILD
  http_directory = "${var.build_root}/prov"
  http_port_max = var.http_port_max
  http_port_min = var.http_port_min
  output_directory = "${var.build_root}/out/${var.os_name}/"
  vm_name = "${var.os_name}-${var.os_version}"

// C&C
  ssh_password = var.prov_pass
  ssh_username = var.prov_user
  ssh_port = 22
  ssh_timeout = "15m"
  ssh_wait_timeout = "300s"
  shutdown_command = "sudo -S shutdown -P now"
  boot_wait = "3s"
  boot_command = [
    "3",
//    "boot -vDS115200<enter>",
    "boot -vs<enter>",
    "<wait15><enter>",
    "mount -rt cd9660 /dev/iso9660/OEMDRV /media/;",
    "/media/inst.sh<enter><wait1>",
    "<leftCtrlOn>d<leftCtrlOff>"
  ]
}


build {
  sources = [
    "source.qemu.freebsd"
  ]

}
