#!/bin/sh
set -x

for dir in /media/union/*; do
	mount -t unionfs $dir /$(basename $dir)
done
