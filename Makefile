include lib.mk

PACKER_ROOT ?= $(PWD)/../.build/packer
PACKER_TAG ?= latest
PACKER_IMG ?= registry.gitlab.com/ors-it-oss/oci/packer:$(PACKER_TAG)

.PHONY: clean run build validate f32 fbsd12 c8

clean:
	set -x
	podman system prune -af
ifdef CLEAN_ALL
	rm -rf $(PACKER_ROOT)
else
	rm -rf $(PACKER_ROOT)/{build-*,tmp,cache/port,cache/*.lock}
endif
	mkdir -p $(PACKER_ROOT)/{etc,tmp,cache}

fbsd12: freebsd-12.1
freebsd-12.1: PACKER_VARS := FreeBSD/vars/freebsd-12.1.pkrvars.hcl
freebsd-12.1: _packer-build-FreeBSD

f32: fedora-32
fedora-32: PACKER_VARS := Red_Hat/vars/fedora-32.pkrvars.hcl
#fedora-32: PACKER_ONLY := pkg
fedora-32: _packer-build-Red_Hat

c8: centos-8
centos-8: PACKER_VARS := Red_Hat/vars/centos-8.pkrvars.hcl
centos-8: _packer-build-Red_Hat

coreos: coreos-stable
coreos-stable: _packer-build-CoreOS

con-%:
	scripts/libexec.sh con $*

#tar:
#	set -x
#	podman --log-level info run \
#	--hostname Packer \
#	--device /dev/kvm \
#	--env PACKER_LOG=1 \
#	--volume $(PWD):/build \
#	--workdir /build/Tar \
#	--volume $(PWD)/.build/etc:/etc/packer.d \
#	--volume $(PWD)/.build/tmp:/var/packer/tmp \
#	--volume $(PWD)/.build/cache:/var/packer/cache \
#     -p 5900:5900 \
#	--rm -ti $(IMG) packer build -var-file=user.json example.json
#	packer build -var-file=user.json example.json

run:
	set -x
	podman --log-level info run \
	--env PACKER_LOG=1 \
	--hostname Packer \
	--device /dev/kvm \
	--volume $(PWD):/build \
	--workdir /build \
	--volume $(PWD)/.build/etc:/etc/packer.d \
	--volume $(PWD)/.build/tmp:/var/packer/tmp \
	--volume $(PWD)/.build/cache:/var/packer/cache \
     -p 5900:5900 \
	--rm -ti $(IMG) bash

c8-validate:
	ksvalidator "Red Hat/oemdrv/ks.cfg"
	cd "Red Hat"; PACKER_LOG=1 packer validate -var-file=vars/centos8.pkrvars.hcl .

f32-validate:
	cd "Red Hat"; PACKER_LOG=1 packer validate -var-file=vars/fedora32.pkrvars.hcl .
