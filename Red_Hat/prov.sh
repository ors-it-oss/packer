#!/bin/bash

authn(){
	cat <<-AUTHN
		rootpw --iscrypted "$(mkpasswd  -m sha-512 -s <<< $ROOT_PASS)"
		user --name=orquestador --uid=800 --homedir=/home/orquestador --iscrypted --password="$(mkpasswd  -m sha-512 -s <<< $PKR_VAR_prov_pass)"
		sshkey --username=orquestador "$(cat "$PKR_PROV_DIR/packer_ssh.pub")"
	AUTHN
	for key in $(ls $PACKER_ROOT/etc/ssh/*); do
		echo "sshkey --username=orquestador \"$(head -n 1 "$key")\""
	done
}

authn >"$PKR_PROV_DIR/ks/authn.cfg"
