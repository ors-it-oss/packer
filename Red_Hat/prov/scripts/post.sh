#!/bin/sh

prep_chroot(){
	header "Mounting OEMDRV in sysimage chroot..."
	mkdir -p /mnt/sysimage/run/install/oem
	mount LABEL=OEMDRV /mnt/sysimage/run/install/oem
}
