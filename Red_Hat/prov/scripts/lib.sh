#------------------------ Common tools ------------------------#
header() {
  # Nicely formatted demarkation line. You're welcome ^^
  cat >&2 <<-TPL


		#------------------------ $1 ------------------------#
		${2+# $2}

TPL
}

fetch() {
  curl -fL${fetch_verbose} "$@"
}
