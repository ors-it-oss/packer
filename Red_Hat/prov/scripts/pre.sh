#!/bin/sh

os_includes(){
	(
		. /etc/os-release
		header "enabling $ID OS kickstart..."
		ln -s /run/install/oem/ks/${ID}.cfg /run/install/os.cfg
	)
}

ks_ssh(){
	if [[ -n "$OH_SSH" ]]; then
		header "Opening SSH during install..."
		ln -s /run/install/oem/ks/ssh.cfg /run/install/ssh.cfg
	else
		header "No SSH during install enabled"
		touch /run/install/ssh.cfg
	fi
}

fw_vars(){
	export packer_url=$(cat /sys/firmware/qemu_fw_cfg/by_name/opt/io.packer/url/raw) || true
}
