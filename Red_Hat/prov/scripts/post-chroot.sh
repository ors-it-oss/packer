#!/bin/sh

sudo_user(){
	header "Install sudoers for $user"
	user="$1"
	cat >"/etc/sudoers.d/$user" <<-SUDOERS
		# Give user $user permissions for sudo
		Defaults: $user !requiretty
		%$user ALL=(ALL) NOPASSWD: ALL
	SUDOERS
	chmod 440 "/etc/sudoers.d/$user"
}

cleanup(){
	header "Cleaning dnf cache"
	dnf clean all

	echo "Removing random-seed so it's not the same in every image."
	rm -f /var/lib/systemd/random-seed
	echo "Cleanup leftover networking configuration"
	rm -f /etc/NetworkManager/system-connections/*.nmconnection

	# Clear machine-id on pre generated images
	truncate -s 0 /etc/machine-id

	header "SElinux relabel"
	restorecon -RrFv /

	journalctl --rotate

	header "fstrim all disks"
	#dd if=/dev/zero of=/EMPTY bs=1M; rm -f /EMPTY
	fstrim -A

	header "forcing sync flushes to all disks"
	sync; sync
}


