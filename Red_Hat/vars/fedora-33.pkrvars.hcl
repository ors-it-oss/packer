iso_checksum = "file:https://dl.fedoraproject.org/pub/fedora/linux/releases/33/Server/x86_64/iso/Fedora-Server-33-1.2-x86_64-CHECKSUM"
iso_url = "https://dl.fedoraproject.org/pub/fedora/linux/releases/33/Server/x86_64/iso/Fedora-Server-netinst-x86_64-33-1.2.iso"
os_name = "Fedora"
os_version = 33
