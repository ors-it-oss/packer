# Tryin' real hard not to depend on https://gmsl.sourceforge.io/
# See the README.md
# TODO: Check https://www.gnu.org/software/make/manual/html_node/Special-Variables.html#Special-Variables
# .RECIPEPREFIX := | w00t!
#
##-------- INIT VARS --------##
ifneq ($(filter $(DEBUG), all make),)
# Don't echo commands unless $DEBUG
.SILENT:
endif

# Execute everything in a single shell
.ONESHELL:

# Delete the default suffixes & rules (make -d)
.SUFFIXES:
%: %,v
%: RCS/%,v
%: RCS/%
%: s.%
%: SCCS/s.%


##-------- PACKER FUNCS --------##
# Run Packer
_packer-build-%:
	set -a
	. $(PACKER_ROOT)/etc/secrets.env
	PACKER_IMG=$(PACKER_IMG)
	PACKER_ROOT=$(PACKER_ROOT)
	PACKER_VARS=$(PACKER_VARS)
	set +a
	scripts/libexec.sh packer_build $*

valid-rh-%: PACKER_VARS := Red_Hat/vars/centos-$*.pkrvars.hcl
valid-rh-%: _packer-build-Red_Hat
valid-rh-%:
	ksvalidator "Red Hat/oemdrv/ks.cfg"

	cd "Red Hat"; PACKER_LOG=1 packer validate -var-file=vars/centos8.pkrvars.hcl .
