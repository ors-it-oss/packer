#!/bin/bash

streams="stable testing"

etl="$(cat <<-'VARS'
	.architectures.x86_64.artifacts.metal | {
		iso_url: .formats.iso.disk.location,
		iso_checksum: .formats.iso.disk.sha256,
		os_name: "Fedora CoreOS",
		os_version: .release
	}
VARS
)"

for rel in $streams; do
	varf="$PKR_PROV_DIR/${rel}.json"
	curl -LsS https://builds.coreos.fedoraproject.org/streams/${rel}.json | \
		jq "$etl" >"$varf"
		export PACKER_VARS="$PACKER_VARS $varf"
done
